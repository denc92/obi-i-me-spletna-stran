# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, session, url_for, request, g, make_response
from flask.ext.login import login_user, logout_user, current_user, login_required
from forms import LoginForm
from app import app, db, lm
from models import User
from datetime import datetime
from sqlalchemy import or_, and_
from flask import jsonify
from functools import wraps
from time import mktime
import json

class JsonEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime):
            return int(mktime(obj.timetuple()))

        return json.JSONEncoder.default(self, obj)

@lm.user_loader
def user_loader(id):
    return User.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')
   
@app.route('/login', methods = ['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(and_(User.username == form.username.data, User.password == form.password.data)).first()
        if not user:
            flash('Napaka pri prijavi')
            return redirect(url_for('login'))
        else:
            remember_me = form.remember_me.data
            if 'remember_me' in session:
                session['remember_me'] = remember_me
                session.pop('remember_me', None)
            login_user(user, remember = remember_me)
            return redirect(url_for('index'))
    return render_template('login.html', 
		form = form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/add', methods = ['GET', 'POST'])
def add():
    longi = request.args.post('longitude')
    lat = request.args.post('latitude')
    acc = request.args.post('accuracy')
    
    
    #location = User(longitude = longi, latitude = lat, accuracy = acc, last_seen = datetime.utcnow())
    db.session.add(location)
    db.session.commit()

@app.route('/map')
@login_required
def map():
    #myOwnRoutes = g.user.get_my_routes()

    return render_template('map.html')

@app.route('/map/get-routes', methods = ['GET', 'POST'])
@login_required
def get_routes():
    myOwnRoutes = g.user.get_my_routes()

    def serialize_point(s):
       return {
			'id': s.id,
            'index': s.index,
            'routeid': s.routeid,
            'description': s.description,
            'add_time': s.add_time,
            'longitude': s.longitude,
            'latitude': s.latitude
       }

    def serialize_route(s):
       return {
			'id': s['route'].id,
            'userid': s['route'].userid,
            'name': s['route'].name,
            'description': s['route'].description,
            'add_time': s['route'].add_time,
            'points': [serialize_point(i) for i in s['points']]
       }

    myRoutes = [serialize_route(i) for i in myOwnRoutes['myOwnRoutes']]

    response = make_response(json.dumps({'myOwnRoutes':myRoutes}, cls=JsonEncoder))
    response.headers['Content-type'] = 'application/json'
    return response
    #return myOwnRoutes

@app.route('/map/add-route', methods = ['GET', 'POST'])
@login_required
def add_route():
    if request.method == "POST":
        routeData = request.json['route']
        data = request.json['points']
        g.user.add_route(routeData, data)
        return jsonify({'data':data})
    return 'false'

################################################################################
#                                 API                                          #
################################################################################

def authenticate():
    
    return jsonify({'username':None,
                    'password':None})

    return abort(401)

def api_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return authenticate()
        
        user = User.query.filter(and_(User.username == auth.username, User.password ==  auth.password)).first()

        if user is None:
            return authenticate()
        
        g.user = user
        
        return f(*args, **kwargs)
    return decorated_function

@app.route("/api/login")
@api_required
def api_login():

    return jsonify({'username':g.user.username,
                    'password':g.user.password})

@app.route('/api/map/update-my-location/<lng>/<lat>/<acc>', methods = ['GET', 'POST'])
@api_required
def update_my_location(lng, lat, acc):
    #lng = request.form.get('longitude')
    #lat = request.form.get('latitude')
    #acc = request.form.get('accuracy')
    
    
    user = User.query.filter_by(id=g.user.id).first()
    user.longitude = lng
    user.latitude = lat
    user.accuracy = acc
    user.last_seen = datetime.utcnow()
    #User(longitude = longi, latitude = lat, accuracy = acc, last_seen = datetime.utcnow())
    db.session.add(user)
    db.session.commit()
    return jsonify({'result':True})

@app.route('/api/map/get-routes', methods = ['GET', 'POST'])
@api_required
def get_routes():
    myOwnRoutes = g.user.get_my_routes()

    def serialize_point(s):
       return {
			'id': s.id,
            'index': s.index,
            'routeid': s.routeid,
            'description': s.description,
            'add_time': s.add_time,
            'longitude': s.longitude,
            'latitude': s.latitude
       }

    def serialize_route(s):
       return {
			'id': s['route'].id,
            'userid': s['route'].userid,
            'name': s['route'].name,
            'description': s['route'].description,
            'add_time': s['route'].add_time,
            'points': [serialize_point(i) for i in s['points']]
       }

    myRoutes = [serialize_route(i) for i in myOwnRoutes['myOwnRoutes']]

    response = make_response(json.dumps(myRoutes, cls=JsonEncoder))
    response.headers['Content-type'] = 'application/json'
    return response

@app.route('/api/map/get-route-points-data/<int:id>', methods = ['GET','POST'])
@api_required
def get_route_point_data(id):
    points = g.user.get_userRoutePointsData(id)

    def serialize_points(s):
       return {
			'id': s.id,
            'pointid': s.pointid,
            'userid': s.userid,
            'visited': s.visited
       }

    myPoints = [serialize_points(i) for i in points['pointsData']]

    response = make_response(json.dumps(myPoints, cls=JsonEncoder))
    response.headers['Content-type'] = 'application/json'
    return response

@app.route('/api/map/visit-routepoint/<int:pointid>', methods = ['GET', 'POST'])
@api_required
def visit_routepoint(pointid):
    data = g.user.visit_point(pointid)
    
    return jsonify({'result':True})

@app.errorhandler(404)
def internal_error(error):
	return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
	db.session.rollback()
	return render_template('500.html'), 500

