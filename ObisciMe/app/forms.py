﻿from flask.ext.wtf import Form, TextField, BooleanField, PasswordField, TextAreaField, FileField
from flask.ext.wtf import Required, Length
from app.models import User

class LoginForm(Form):
    username = TextField('username', validators = [Required()])
    password = PasswordField('password', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)