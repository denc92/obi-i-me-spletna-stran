# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import select
import hashlib
from app import app, db
from config import DB_URI
from sqlalchemy import or_, and_
from datetime import datetime
from flask import jsonify
from werkzeug._internal import _log
from sqlalchemy import create_engine


engine = create_engine(DB_URI, echo=True)


class User(db.Model):
    __tablename__ ='user'

    id = db.Column('id', db.Integer, primary_key = True)
    username = db.Column('username', db.String(64), unique = True)
    password = db.Column('password', db.String(64))
    email = db.Column('email', db.String(120), index = True, unique = True)
    #role = db.Column(db.SmallInteger, default = ROLE_USER)
    about_me = db.Column('about_me', db.String(140))
    last_seen = db.Column('last_seen', db.DateTime)

    longitude = db.Column('longitude', db.String(255))
    latitude = db.Column('latitude', db.String(255))
    accuracy = db.Column('accuracy', db.String(255))

    route_author = db.relationship('Route', backref='user')
    my_routes = db.relationship('Route', secondary='user_route')
    visited_points = db.relationship('Routepoint', secondary='user_routepoint')

    def get_my_routes(self):

        routes = db.session.execute("""select * from route where route.userid=:user""", {'user':self.id}).fetchall()
        route_s = []
        for route in routes:
            points = routes = db.session.execute("""select * from routepoint where routepoint.routeid=:route""", {'route':route.id}).fetchall()

            route_s.append({
                'route': route,
                'points': points
            })

        return {'myOwnRoutes': route_s}

    def get_userRoutePointsData(self, id):
        points = db.session.execute("""select u.* from user_routepoint u, routepoint r
                                    where u.pointid=r.id AND r.routeid=:pointid AND u.userid=:user""", 
                                    {'user':self.id, 'pointid':id}).fetchall()
        
        return {'pointsData': points}

    def visit_point(self, pointid):
        data = User_routepoint.query.filter(and_(User_routepoint.userid == self.id, User_routepoint.pointid == pointid)).first()

        if data:
            return {'data':"true"}

        point = User_routepoint(userid = self.id, pointid = pointid, visited = datetime.utcnow())

        db.session.add(point)
        db.session.commit()

        return {'data': "true"}

    def add_route(self, routeData, data):

        #add route
        start_point = Route(userid = self.id, name = routeData['name'], description = routeData['description'], add_time = datetime.utcnow())
        
        db.session.add(start_point)
        db.session.commit()
        db.session.refresh(start_point)
        
        #add user_route
        user_r = User_route(routeid = start_point.id, userid = self.id, add_time = datetime.utcnow())
        db.session.add(user_r)
        db.session.commit()

        for d in data:
            #add point
            point = Routepoint(index = d['index'], routeid = start_point.id, description = d['description'], add_time = datetime.utcnow(),
                                longitude = d['longitude'], latitude = d['latitude'])
            db.session.add(point)
            db.session.commit()
            db.session.refresh(point)

        return True

    
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return 

    def get_id(self):
        return unicode(self.id)


class User_route(db.Model):
    __tablename__ = 'user_route'

    id = db.Column('id', db.Integer, primary_key = True)
    routeid = db.Column('routeid', db.Integer(255), db.ForeignKey('route.id'))
    userid = db.Column('userid', db.Integer(255), db.ForeignKey('user.id'))
    add_time = db.Column('add_time', db.DateTime)
    completed = db.Column('completed', db.DateTime)

class User_routepoint(db.Model):
    __tablename__ = 'user_routepoint'

    id = db.Column('id', db.Integer, primary_key = True)
    pointid = db.Column('pointid', db.Integer(255), db.ForeignKey('routepoint.id'))
    userid = db.Column('userid', db.Integer(255), db.ForeignKey('user.id'))
    visited = db.Column('visited', db.DateTime)

class Route(db.Model):
    __tablename__ = 'route'

    id = db.Column('id', db.Integer, primary_key = True)
    userid = db.Column('userid', db.Integer(255), db.ForeignKey('user.id'))
    name = db.Column('name', db.String(255))
    description = db.Column('description', db.String(255))
    add_time = db.Column('add_time', db.DateTime)
    #longitude = db.Column('longitude', db.String(255))
    #latitude = db.Column('latitude', db.String(255))

    users = db.relationship('User', secondary='user_route')
    points = db.relationship('Routepoint', backref='route')

class Routepoint(db.Model):
    __tablename__ = 'routepoint'

    id = db.Column('id', db.Integer, primary_key = True)
    index = db.Column('index', db.Integer)
    routeid = db.Column('routeid', db.Integer(255), db.ForeignKey('route.id'))
    description = db.Column('description', db.String(255))
    add_time = db.Column('add_time', db.DateTime)
    longitude = db.Column('longitude', db.String(255))
    latitude = db.Column('latitude', db.String(255))

    user = db.relationship('User', secondary='user_routepoint')
