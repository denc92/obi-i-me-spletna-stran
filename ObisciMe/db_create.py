#!flask/bin/python
from migrate.versioning import api
from config import DB_URI
from config import SQLALCHEMY_MIGRATE_REPO
from app import db
import os.path
db.create_all()
if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
	api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
	api.version_control(DB_URI, SQLALCHEMY_MIGRATE_REPO)
else:
	api.version_control(DB_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCEHMY_MIGRATE_REPO))