from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
user_routepoint = Table('user_routepoint', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('pointid', Integer),
    Column('userid', Integer),
    Column('visited', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user_routepoint'].columns['pointid'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user_routepoint'].columns['pointid'].drop()
