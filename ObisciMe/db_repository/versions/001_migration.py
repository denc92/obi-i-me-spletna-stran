from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
route = Table('route', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('userid', Integer),
    Column('name', String(length=255)),
    Column('description', String(length=255)),
    Column('add_time', DateTime),
    Column('longitude', String(length=255)),
    Column('latitude', String(length=255)),
)

routepoint = Table('routepoint', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('routeid', Integer),
    Column('description', String(length=255)),
    Column('add_time', DateTime),
    Column('longitude', String(length=255)),
    Column('latitude', String(length=255)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('username', String(length=64)),
    Column('password', String(length=64)),
    Column('email', String(length=120)),
    Column('about_me', String(length=140)),
    Column('last_seen', DateTime),
    Column('longitude', String(length=255)),
    Column('latitude', String(length=255)),
    Column('accuracy', String(length=255)),
)

user_route = Table('user_route', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('routeid', Integer),
    Column('userid', Integer),
    Column('add_time', DateTime),
    Column('completed', DateTime),
)

user_routepoint = Table('user_routepoint', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('routeid', Integer),
    Column('userid', Integer),
    Column('visited', DateTime),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['route'].create()
    post_meta.tables['routepoint'].create()
    post_meta.tables['user'].create()
    post_meta.tables['user_route'].create()
    post_meta.tables['user_routepoint'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['route'].drop()
    post_meta.tables['routepoint'].drop()
    post_meta.tables['user'].drop()
    post_meta.tables['user_route'].drop()
    post_meta.tables['user_routepoint'].drop()
