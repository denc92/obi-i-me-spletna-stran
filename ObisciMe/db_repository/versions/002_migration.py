from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
routepoint = Table('routepoint', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('index', Integer),
    Column('routeid', Integer),
    Column('description', String(length=255)),
    Column('add_time', DateTime),
    Column('longitude', String(length=255)),
    Column('latitude', String(length=255)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['routepoint'].columns['index'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['routepoint'].columns['index'].drop()
