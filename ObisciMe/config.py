# -*- coding: utf-8 -*-
import os
import md5

basedir = os.path.abspath(os.path.dirname(__file__))

dbhost = '127.0.0.1'
dbuser = 'root'
dbpass = 'password'
dbname = 'mylocations'
 
DB_URI = 'mysql://' + dbuser + ':' + dbpass + '@' + dbhost + '/' +dbname
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')



########################################################################
#                       Delete:D                                       #
########################################################################
#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

CSRF_ENABLED = True
SECRET_KEY = 'yoo-will-never-guess'

